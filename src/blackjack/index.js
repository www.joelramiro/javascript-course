import _ from 'underscore';
import {crearDeck, pedirCarta, turnoComputadora,crearCarta,acumularPuntos} from './usecases'

/**
 *  2C=2 DE TREBOLES
 *  2D=2 DE DIAMANTES
 *  2H=2 DE CORAZONES
 *  2S=2 DE ESPADAS
 * */

const moduloJuego = (() => {
  'use strict' //Le dice que sea rigoroso con revisiones de codigo

  let deck = [],
      puntosJugadores = [];

  const tipos = ['C','D','H','S'],
        especiales = ['A','J','Q','K'];
  
  const btnPedir = document.getElementById('btnPedir'),
        btnNuevoJuevo = document.getElementById('btnNuevoJuego'),
        btnDetener = document.getElementById('btnDetener');
  
  const divCartasJugador = document.querySelectorAll('.divCartas'),
        puntosHTML = document.querySelectorAll('small');

  const initializeGame = (numeroJugadores = 2) =>{
      deck = crearDeck(tipos,especiales);
      puntosJugadores = [];
      for (let i = 0; i < numeroJugadores; i++) {
          puntosJugadores.push(0);            
      }

      btnPedir.disabled = false;
      btnDetener.disabled = false;

      for (let i = 0; i < puntosJugadores.length; i++) {
          puntosJugadores[i] = 0;            
      }        
      puntosHTML.forEach(element => element.innerText = 0);
      divCartasJugador.forEach(element => element.innerHTML = '');
  }    

  
  
  btnPedir.addEventListener('click', () => {
      let cartaPedida = pedirCarta(deck);
      let puntosJugador = acumularPuntos(0,cartaPedida,puntosJugadores,puntosHTML);
      crearCarta(0,cartaPedida,divCartasJugador);
      puntosHTML[0].innerText=puntosJugadores[0];

      if(puntosJugador >=21)
      {
          btnPedir.disabled = true;
          btnDetener.disabled = true;
          turnoComputadora(puntosJugadores[0],deck,divCartasJugador,puntosJugadores,puntosHTML);
          determinarGanador();
      }
  });
  
  btnDetener.addEventListener('click', ()=>{
      btnPedir.disabled = true;
      btnDetener.disabled = true;
      turnoComputadora(puntosJugadores[0],deck,divCartasJugador,puntosJugadores,puntosHTML);    
      determinarGanador();
  });
  
  

  const determinarGanador = ()=> {
      let puntosComputadora = puntosJugadores[puntosJugadores.length-1];

      setTimeout(() => {
          if(puntosComputadora >21)
          { 
              alert('Felicidades, ganó!!!')
          }
          else  if(puntosComputadora == puntosJugadores[0])
          {
              alert('Empate!!!')
          }
          else
          {
              alert('Lo siento mucho, ha perdido!!!')
          }
      }, 10);

  }

  
  
  btnNuevoJuevo.addEventListener('click', ()=>{
      initializeGame();
  });      

  return {
      nuevoJuego: initializeGame,
  };
})();
