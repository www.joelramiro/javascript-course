
/**
 * Crea la carta
 *@param {Number} turno turno actual
 *@param {String} cartaPedida la carta a crear
 *@param {div} divCartasJugador el div donde se insertara la carta
*/

export const crearCarta = (turno,cartaPedida,divCartasJugador) => {
    const newCarta = document.createElement('img');
    newCarta.classList.add('carta');
    newCarta.src=`assets/cartas/${cartaPedida}.png`;
    divCartasJugador[turno].append(newCarta);
}