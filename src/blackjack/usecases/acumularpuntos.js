import {valorCarta} from './'

/**
 * Acumula puntos
 *@param {Number} turno turno a realizar
 *@param {String} cartaPedida la carta a crear
 *@param {Number} puntosJugadores el numero de puntos de los jugadores
 *@param {div} puntosHTML div para acumular puntos
 *@return {Number} retorna puntos acumulados
 */
 export const acumularPuntos = (turno,cartaPedida,puntosJugadores,puntosHTML) => {
    puntosJugadores[turno] += valorCarta(cartaPedida);
    puntosHTML[turno].innerText=puntosJugadores[turno];
    return puntosJugadores[turno];
}