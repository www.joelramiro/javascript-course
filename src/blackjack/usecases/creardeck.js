import _ from 'underscore';

/**
 * Devuelve el mazo de cartas
 *@param {Array<String>} tiposDeCarta ejemplo ['C','D','H','S']
 *@param {Array<String>} tiposEspeciales - ejemplo ['A','J','Q','K']
 *@returns {Array<String>} Retorna un nuevo deck de cartas 
*/
export const crearDeck = (tiposDeCarta,tiposEspeciales) => {

    if(!tiposDeCarta || tiposDeCarta.length === 0) throw new Error('Tipos de carta es requerido')

    if(!tiposEspeciales || tiposEspeciales.length === 0) throw new Error('Tipos de carta especiales es requerido')

    let deck = [];

    for (let i = 2; i<=10;i++)
    {
        for(let tipo of tiposDeCarta)
        {
            deck.push(i + tipo);
        }
    }
    for(let tipo of tiposDeCarta)
    {
        for(let tipoEspecial of tiposEspeciales)
        {
            deck.push(tipoEspecial + tipo);
        }
    }

     return _.shuffle(deck);
}
