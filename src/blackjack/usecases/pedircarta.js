/**
 * Devuelve una nueva carta
 *@param {Array<String>} deck el mazo de cartas
 *@returns {String} Retorna una nueva carta 
*/

export const pedirCarta = (deck) =>{
    if(!deck || deck.length == 0)
    {
        throw 'No hay cartas en la baraja';
    }  

    var randomIndex = Math.floor(Math.random() * deck.length);
    return deck.splice(randomIndex, 1)[0]; // Elimina y obtén el elemento
}