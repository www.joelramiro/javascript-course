import {pedirCarta as nuevaCarta, acumularPuntos, crearCarta} from './'

/**
 * Realiza el turno de la computadora
 *@param {Number} min puntos minimos que la computadora necesita para ganar
 *@param {Array<String>} deck la baraja de cartas
 *@param {Number} puntosJugadores el numero de puntos de los jugadores
 *@param {div} puntosHTML div para acumular puntos
 *@param {div} divCartasJugador el div donde se insertara la carta
*/

export const turnoComputadora = (min, deck,divCartasJugador,puntosJugadores,puntosHTML) => {
    
    if(!min) throw new Error('Puntos minimos son necesarios')
    if(!divCartasJugador) throw new Error('El div es requerido')
    
    if(!deck || deck.length == 0)
    {
        throw 'No hay cartas en la baraja';
    }
     
    let puntosComputadora = 0;
    do{
        let cartaPedida = nuevaCarta(deck);
        puntosComputadora = acumularPuntos(puntosJugadores.length-1,cartaPedida, puntosJugadores,puntosHTML);
        crearCarta(puntosJugadores.length-1,cartaPedida,divCartasJugador);
    }
    while( min<=21 &&(puntosComputadora < min &&puntosComputadora<21))   
}

